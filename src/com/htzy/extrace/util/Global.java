package com.htzy.extrace.util;

import org.apache.commons.codec.binary.Base64;

public class Global {
	/**
	 * 本地服务器地址
	 */
//	public static final String HOSTURL = "http://192.168.191.1:8082/extraceServer_1/cxf/rest";
	public static final String HOSTURL = "http://192.168.191.1:8080/extraceServer_1";
	/**
	 * coding服务器地址
	 */
	public static final String CODING_URL = "http://extrace.coding.io";
	
	/**
	 * 开源中国服务器地址
	 */
	public static final String OSCHINA_URL = "不支持oschina";

	/**
	 * 访问服务器路径等运行配置问文件信息
	 */
	public static final String PREFS_NAME = "ExTrace.cfg";

	/**
	 * 登陆用户信息保存配置文件
	 */
	public static final String CONFIG_USER = "UserInfo.cfg";

	/**
	 * 是否为debug模式
	 */
	public static final boolean ISDEUBG = true;
	
	/**
	 * 百度实时位置上传时间间隔
	 */
	public static final int TIME = 5000;

	/**
	 * base64加密
	 * 
	 * @author 黄诗鹤
	 */
	public static String encodeStr(String str) {
		byte[] b = new Base64().encode(str.getBytes());
		return new String(b);
	}

	/**
	 * base64解密
	 * 
	 * @author 黄诗鹤
	 */
	public static String decodeStr(String str) {
		byte[] b = new Base64().decode(str.getBytes());
		return new String(b);
	}

}
