package com.htzy.extrace.loader;

import android.app.Activity;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.UserInfo;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.ui.main.ExTraceApplication;

public class UserInfoLoader extends HttpAsyncTask {

	String url;
	IDataAdapter<UserInfo> adapter;
	private Activity context;

	public UserInfoLoader(IDataAdapter<UserInfo> adpt, Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication) context.getApplication())
				.getMiscServiceUrl();
	}

	@Override
	public void onDataReceive(String class_data, String json_data) {
		System.out.println("json_data::" + json_data);
		if (json_data == null || json_data.length() == 0) {
			// 没有登陆成功
			adapter.setData(null);
			adapter.notifyDataSetChanged();
		} else {
			// 登陆成功
			UserInfo ci = JsonUtils.fromJson(json_data,
					new TypeToken<UserInfo>() {
					});
			adapter.setData(ci);
			adapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		Log.i("onStatusNotify", "onStatusNotify: " + str_response);
	}

	/**
	 * 登陆
	 * 
	 * @param userInfo
	 * @author renyuzhuo
	 */
	public Boolean login(UserInfo userInfo) {
		url = ((ExTraceApplication)context.getApplication()).getLoginServiceUrl();
		url += "login?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	} 
	/**
	 * 加密登录信息实现登录
	 * by chenwenyan
	 * @param userInfo
	 * @return
	 */
	public Boolean loginTest(UserInfo userInfo) {
		url += "loginTest?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
    /**
     * 修改派送包裹ID
     * @param userInfo
     * @return
     */
	public Boolean delivePackageEdit(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="delivePackageEdit?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
    /**
     * 修改揽收包裹ID
     * @param userInfo
     * @return
     */
	public Boolean receivePackageEdit(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="receivePackageEdit?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
    /**
     * 修改转运包裹ID
     * @param userInfo
     * @return
     */
	public Boolean transPackageEdit(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="transPackageEdit?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
/**
 * 删除派送包裹ID
 * @param userInfo
 * @return
 */
	public Boolean delivePackageDelete(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="delivePackageDelete?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
	/**
	 * 删除揽收包裹ID
	 * @param userInfo
	 * @return
	 */
	public Boolean receivePackageDelete(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="receivePackageDelete?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}
	/**
	 * 删除转运包裹ID
	 * @param userInfo
	 * @return
	 */
	public Boolean transPackageDelete(UserInfo userInfo) {
		// TODO Auto-generated method stub
		url+="transPackageDelete?_type=json";
		String jsonObj = JsonUtils.toJson(userInfo, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return  true;
	}

}
