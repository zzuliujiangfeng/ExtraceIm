package com.htzy.extrace.loader;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.ExpressSheet;
import com.htzy.extrace.misc.model.Posion;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.ui.main.ExTraceApplication;
import com.htzy.extrace.util.Global;

public class ExpressLoader extends HttpAsyncTask {

	String url;
	IDataAdapter<ExpressSheet> adapter;
	private Activity context;

	public ExpressLoader(IDataAdapter<ExpressSheet> adpt, Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication) context.getApplication())
				.getDomainServiceUrl();
	}

	/**
	 * 接受服务器返回的快件信息
	 */
	@Override
	public void onDataReceive(String class_name, String json_data) {
		switch (class_name)
			{
			case "NullReceivePackage": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "您没有揽收包裹，请检查后重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "ExpressSheet": {
				ExpressSheet ci = JsonUtils.fromJson(json_data,
						new TypeToken<ExpressSheet>() {
						});
				adapter.setData(ci);
				adapter.notifyDataSetChanged();
				break;
			}
			case "E_ExpressSheet": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "此快递单已被使用，请更换其他快递单:(",
						Toast.LENGTH_SHORT).show();
				break;
			}
			case "N_ExpressSheet": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "快件运单已发出!不可更改信息:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "R_ExpressSheet": {
				ExpressSheet ci = JsonUtils.fromJson(json_data,
						new TypeToken<ExpressSheet>() {
						});
				adapter.getData().setID(ci.getID());
				adapter.getData().onSave();
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "快件运单信息保存完成!", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "ReceivePackageOutOfDate": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "您的揽收包裹已过期,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "TransPackageOutOfDate": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "您的转运包裹已过期,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "DelivePackageOutOfDate": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "您的派送包裹已过期,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "ErrExpress": {
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "快件信息有误,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "SendSuccess": {
				Toast.makeText(context, "派送成功:)", Toast.LENGTH_SHORT).show();
				break;
			}
			case "SignedSuccess": {
				Toast.makeText(context, "签收成功:)", Toast.LENGTH_SHORT).show();
				break;
			}
			default:
				break;
			}
	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
	}

	/**
	 * 查询已有快件信息
	 * 
	 * 访问的URL
	 * http://localhost:8080/extraceServer_1/cxf/rest/Domain/getExpressSheet/id号
	 * 如： http://localhost:8080/extraceServer_1/cxf/rest/Domain/getExpressSheet/
	 * 368220974003 将快件id发送到服务器，请求服务器返回该快件的信息
	 */
	public void loadExpress(String id) {
		String url = ((ExTraceApplication) context.getApplication())
				.getServerUrl() + "/cxf/rest1/Domain/";
		url += "getExpressSheet/" + id + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * 访问的URL
	 * http://localhost:8082/extraceServer_1/REST/Domain/newExpressSheet/id
	 * /{id}/uid/{uid} 新建快件
	 * 通过快件Id向服务器发送快件信息，将快件（expressheet）的id和用户（userinfo）id进行连接，
	 */
	public void newExpress(String id) {
		int uid = ((ExTraceApplication) context.getApplication())
				.getLoginUser().getUID();
		url += "newExpressSheet/id/" + id + "/uid/" + uid + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void save(ExpressSheet es) {
		String jsonObj = JsonUtils.toJson(es, true);
		url += "saveExpressSheet";
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
