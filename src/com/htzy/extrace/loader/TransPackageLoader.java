package com.htzy.extrace.loader;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.TransPackage;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.ui.main.ExTraceApplication;

public class TransPackageLoader extends HttpAsyncTask {

	String url;
	IDataAdapter<TransPackage> adapter;
	private Activity context;

	public TransPackageLoader(IDataAdapter<TransPackage> adpt, Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication) context.getApplication())
				.getDomainServiceUrl();
	}

	@Override
	public void onDataReceive(String class_name, String json_data) {
		switch (class_name)
			{
			case "TransPackage": {
				TransPackage ci = JsonUtils.fromJson(json_data,
						new TypeToken<TransPackage>() {
						});
				adapter.setData(ci);
				adapter.notifyDataSetChanged();
				break;
			}
			case "R_TransPackage": {
				// 保存完成
				TransPackage ci = JsonUtils.fromJson(json_data,
						new TypeToken<TransPackage>() {
						});
				adapter.getData().setID(ci.getID());
				adapter.getData().onSave();
				adapter.notifyDataSetChanged();
				Toast.makeText(context, "包裹信息保存完成!", Toast.LENGTH_SHORT).show();
				break;
			}
			case "Unpack": {
				adapter.notifyDataSetChanged();
				break;
			}
			case "NoAuthority": {
				Toast.makeText(context, "您无权拆包!:(", Toast.LENGTH_SHORT).show();
				break;
			}
			case "NoManager": {
				Toast.makeText(context, "无此管理员:(", Toast.LENGTH_SHORT).show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "TransPackageOutOfDate": {
				Toast.makeText(context, "转运包裹已过期:(", Toast.LENGTH_SHORT).show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "ErrExpress": {
				Toast.makeText(context, "快件信息有误，操作失败:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "ErrPackage": {
				Toast.makeText(context, "包裹信息有误，操作失败:(", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "ErrTransPackageContent": {
				Toast.makeText(context, "包裹于快件之间联系出错，请重试:(", Toast.LENGTH_SHORT)
						.show();
				break;
			}
			case "NoUser": {
				Toast.makeText(context, "用户信息有误，请重新登录重试:(", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "DelivePackageOutOfDate": {
				Toast.makeText(context, "派送包裹已过期,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "ReceivePackageOutOfDate": {
				Toast.makeText(context, "揽收包裹已过期,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
				break;
			}
			case "ErrDelivePackage": {
				Toast.makeText(context, "派送包裹出错,请检查重试:(", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
			}
			case "SendSuccess": {
				Toast.makeText(context, "派送快件成功:)", Toast.LENGTH_SHORT).show();
			}
			case "SignedPackageOutOfDate": {
				Toast.makeText(context, "派送包裹过期:(", Toast.LENGTH_SHORT).show();
			}
			case "ErrSignedPackage": {
				Toast.makeText(context, "派送包裹出错:(", Toast.LENGTH_SHORT).show();
			}
			}
	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		// TODO Auto-generated method stub

	}

	public void Load(String id) {
		url += "getTransPackage/" + id + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void Save(TransPackage tp) {
		String jsonObj = JsonUtils.toJson(tp, true);
		url += "saveTransPackage";
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void pack(String expressheetID) {
		url = url + "pack/" + ExTraceApplication.getUserInfo().getUID() + "/"
				+ expressheetID + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			Toast.makeText(context, "打包失败！！", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * 拆包
	 * 
	 * @param transpackage
	 *            要拆包的转运包裹
	 * @param sheet
	 *            要拆包的包裹中的快件
	 */
	public void unPack(String transpackage, String sheet) {
		url = url + "unpack/" + ExTraceApplication.getUserInfo().getUID() + "/"
				+ transpackage + "/" + sheet + "/1/2" + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			Toast.makeText(context, "拆包失败！！", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * 派送
	 * 
	 * @param expressheetID
	 *            快件id
	 */
	public void send(String expressheetID) {
		url = url + "send/" + ExTraceApplication.getUserInfo().getUID() + "/"
				+ expressheetID + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			Toast.makeText(context, "派送失败！！", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * 签收
	 * 
	 * @param expressheetID
	 */
	public void signed(String expressheetID) {
		url = url + "signed/" + ExTraceApplication.getUserInfo().getUID() + "/"
				+ expressheetID + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			Toast.makeText(context, "签收失败！！", Toast.LENGTH_LONG).show();
		}
	}
}
