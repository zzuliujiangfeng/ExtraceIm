package com.htzy.extrace.loader;

import java.util.List;

import android.app.Activity;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.HistoryGist;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.ui.main.ExTraceApplication;

public class ExpressHistoryLoader  extends HttpAsyncTask{

	String url;
	IDataAdapter<List<HistoryGist>> adapter;
	private Activity context;

	public ExpressHistoryLoader(IDataAdapter<List<HistoryGist>> adpt, Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication)context.getApplication()).getDomainServiceUrl();
	}

	@Override
	public void onDataReceive(String class_data, String json_data) {
		List<HistoryGist> hg = JsonUtils.fromJson(json_data,
				new TypeToken<List<HistoryGist>>() {
				});
		adapter.setData(hg);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		Log.i("onStatusNotify", "onStatusNotify: " + str_response);
	}

	public void getHistory(String expressId) {
		url += "searchExpressHistory/" + expressId + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}