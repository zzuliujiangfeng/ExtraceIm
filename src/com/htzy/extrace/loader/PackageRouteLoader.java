package com.htzy.extrace.loader;

import java.util.List;

import android.app.Activity;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.PackageRoute;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.ui.main.ExTraceApplication;

/**
 * 包裹位置信息
 * @author liujiangfeng
 */
public class PackageRouteLoader extends HttpAsyncTask {

	String url;
	IDataAdapter<List<PackageRoute>> adapter;

	public PackageRouteLoader(IDataAdapter<List<PackageRoute>> adpt,
			Activity context) {
		super(context);
		adapter = adpt;
		url = ((ExTraceApplication) context.getApplication())
				.getDomainServiceUrl();
	}

	@Override
	public void onDataReceive(String class_data, String json_data) {
		// 保存完成
		if (class_data.equals("A_PackageRoute")) {
			// Toast.makeText(context, "实时位置更新完成!", Toast.LENGTH_SHORT).show();
			System.out.println("实时位置更新完成");
		}
		if (json_data == null || json_data.length() == 0) {
			//
			adapter.setData(null);
			adapter.notifyDataSetChanged();
		} else {
			//
			PackageRoute pr = JsonUtils.fromJson(json_data,
					new TypeToken<PackageRoute>() {
					});
			adapter.setData(pr);
			adapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		Log.i("onStatusNotify", "onStatusNotify: " + str_response);

	}

	/**
	 * 位置信息上传
	 */
	public void uploardRoute(PackageRoute packageRoute) {
		url += "uploardRoute?_type=json";
		String jsonObj = JsonUtils.toJson(packageRoute, true);
		try {
			execute(url, "POST", jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 传输定位到经纬度
	 */
	public void saveCurrentPosition(float x, float y) {
		int uid = ExTraceApplication.getUserInfo().getUID();
		url = url + "addPackageRoute/" + uid + "/" + x + "/" + y
				+ "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
