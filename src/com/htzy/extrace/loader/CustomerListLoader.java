package com.htzy.extrace.loader;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.htzy.extrace.misc.model.CustomerInfo;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.ui.main.ExTraceApplication;

public class CustomerListLoader extends HttpAsyncTask {

	String url;
	IDataAdapter<List<CustomerInfo>> adapter;
	private Activity context;

	public CustomerListLoader(IDataAdapter<List<CustomerInfo>> adpt,
			Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication) context.getApplication())
				.getMiscServiceUrl();
	}

	@Override
	public void onDataReceive(String class_data, String json_data) {
		if (json_data.equals("Deleted")) {
			// 这个地方不好处理
			// adapter.getData().remove(0);
			Toast.makeText(context, "客户信息已删除!", Toast.LENGTH_SHORT).show();
		} else {
			if (json_data == null || json_data.length() == 0) {
				Toast.makeText(context, "没有符合条件的客户信息!", Toast.LENGTH_SHORT)
						.show();
				adapter.setData(null);
				adapter.notifyDataSetChanged();
			} else {
				List<CustomerInfo> cstm = JsonUtils.fromJson(json_data,
						new TypeToken<List<CustomerInfo>>() {
						});
				adapter.setData(cstm);
				adapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		Log.i("onStatusNotify", "onStatusNotify: " + str_response);
	}

	public void LoadCustomerListByTelCode(String telCode) {
		url += "getCustomerListByTelCode/" + telCode + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void LoadCustomerListByName(String name) {
		url += "getCustomerListByName/" + name + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DeleteCustomer(int id) {
		url += "deleteCustomerInfo/" + id + "?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getAllCustomer(){
		url += "getAllCustomer?_type=json";
		try {
			execute(url, "GET");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
