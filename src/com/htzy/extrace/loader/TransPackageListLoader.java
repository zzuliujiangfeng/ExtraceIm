package com.htzy.extrace.loader;

import java.util.List;

import com.htzy.extrace.misc.model.TransPackage;
import com.htzy.extrace.net.HttpAsyncTask;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.net.JsonUtils;
import com.htzy.extrace.net.HttpResponseParam.RETURN_STATUS;
import com.htzy.extrace.ui.main.ExTraceApplication;

import android.app.Activity;
import android.widget.Toast;

public class TransPackageListLoader extends HttpAsyncTask{

	String url;
	IDataAdapter<List<TransPackage>> adapter;
	private Activity context;
	
	public TransPackageListLoader(IDataAdapter<List<TransPackage>> adpt, Activity context) {
		super(context);
		this.context = context;
		adapter = adpt;
		url = ((ExTraceApplication)context.getApplication()).getDomainServiceUrl();
	}

	@Override
	public void onDataReceive(String class_name, String json_data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusNotify(RETURN_STATUS status, String str_response) {
		// TODO Auto-generated method stub
		
	}
}
