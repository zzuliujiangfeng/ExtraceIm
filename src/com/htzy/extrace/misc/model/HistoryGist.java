/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package com.htzy.extrace.misc.model;

import java.io.Serializable;
import java.util.Date;

public class HistoryGist implements Serializable {
	private static final long serialVersionUID = 4710165589638194654L;

	public HistoryGist() {
	}

	private Date time;

	private String nodeName;//网店名称

	private String manager;//操作员

	private String tel;

	private float x;

	private float y;

	private int state;

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("HistoryGist[");
		sb.append("Time=").append(getTime()).append(" ");
		sb.append("NodeName=").append(getNodeName()).append(" ");
		sb.append("Manager=").append(getManager()).append(" ");
		sb.append("Tel=").append(getTel()).append(" ");
		sb.append("X=").append(getX()).append(" ");
		sb.append("Y=").append(getY()).append(" ");
		sb.append("State=").append(getState()).append(" ");
		sb.append("]");
		return sb.toString();
	}

	public static final class STATE {
		/**
		 * 揽收
		 */
		public static final int HISTORYGIST_RECEIVE = 0;

		/**
		 * 转运
		 */
		public static final int HISTORYGIST_TRANS = 1;

		/**
		 * 实时位置
		 */
		public static final int HISTORYGIST_NOW = 2;

		/**
		 * 终点位置
		 */
		public static final int HISTORYGIST_TARGET = 3;
	}
}
