package com.htzy.extrace.misc.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.gson.annotations.Expose;

public class PackageRoute implements Serializable, List<PackageRoute> {
	/**
	 * 快递员所带包裹的位置信息
	 * by chenwenyan
	 */
	private static final long serialVersionUID = -14529992698589068L;

	@Expose
	private int SN;
	@Expose
	private String packageId;
	@Expose
	private float x;
	@Expose
	private float y;
	@Expose
	private Date tm;

	public int getSN() {
		return SN;
	}

	public void setSN(int sN) {
		SN = sN;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public Date getTm() {
		return tm;
	}

	public void setTm(Date tm) {
		this.tm = tm;
	}

	public String toString() {
		return toString(false);
	}

	public String toString(boolean SNOnly) {
		if (SNOnly) {
			return String.valueOf(getSN());
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append("PackageRoute[ ");
			sb.append("SN=").append(getSN()).append(" ");
			sb.append("PackageId=").append(getPackageId()).append(" ");
			sb.append("X=").append(getX()).append(" ");
			sb.append("Y=").append(getY()).append(" ");
			sb.append("tm=").append(getTm()).append(" ");
			sb.append("]");
			return sb.toString();
		}
	}

	@Override
	public void add(int location, PackageRoute object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean add(PackageRoute object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int location,
			Collection<? extends PackageRoute> collection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends PackageRoute> collection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> collection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public PackageRoute get(int location) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int indexOf(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<PackageRoute> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<PackageRoute> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<PackageRoute> listIterator(int location) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageRoute remove(int location) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> collection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> collection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public PackageRoute set(int location, PackageRoute object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<PackageRoute> subList(int start, int end) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] array) {
		// TODO Auto-generated method stub
		return null;
	}
}
