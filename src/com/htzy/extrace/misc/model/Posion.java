package com.htzy.extrace.misc.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class Posion implements Serializable {
	private static final long serialVersionUID = 7005920925141618139L;

	public Posion() {
	}

	@Expose
	private int posCode;

	@Expose
	private float x;

	@Expose
	private float y;

	public void setPosCode(int value) {
		this.posCode = value;
	}

	public int getPosCode() {
		return posCode;
	}

	public int getORMID() {
		return getPosCode();
	}

	public void setX(float value) {
		this.x = value;
	}

	public float getX() {
		return x;
	}

	public void setY(float value) {
		this.y = value;
	}

	public float getY() {
		return y;
	}

	public String toString() {
		return toString(false);
	}

	public String toString(boolean idOnly) {
		if (idOnly) {
			return String.valueOf(getPosCode());
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append("Posion[ ");
			sb.append("PosCode=").append(getPosCode()).append(" ");
			sb.append("X=").append(getX()).append(" ");
			sb.append("Y=").append(getY()).append(" ");
			sb.append("]");
			return sb.toString();
		}
	}

}
