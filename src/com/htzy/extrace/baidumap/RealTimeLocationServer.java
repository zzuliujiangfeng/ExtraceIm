package com.htzy.extrace.baidumap;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.htzy.extrace.ui.main.ExTraceApplication;
import com.htzy.extrace.ui.main.LoginActivity;

/**
 * 百度地图：实时位置传输
 * 		      开启系统后，将实时位置定时传输至服务器
 * @author liujiangfeng
 *
 */

public class RealTimeLocationServer extends Service {

	private LocationClient mLocationClient = null;
	private MyLocationListenner myListener = new MyLocationListenner();
	public static String TAG = "msg";

	public float array[] = { 0, 0 };
	sListenner d;

	@Override
	public void onCreate() {

		mLocationClient = new LocationClient(this);
		mLocationClient.registerLocationListener(myListener);
		setLocationOption();// 设定定位参数
		mLocationClient.start();// 开始定位
		d = new sListenner();

	}

	// 设置相关参数
	private void setLocationOption() {
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(ExTraceApplication.getTIME());// 设置发起定位请求的间隔时间为5000ms
		mLocationClient.setLocOption(option);
	}

	@Override
	public void onDestroy() {
		mLocationClient.stop();// 停止定位
		super.onDestroy();
	}

	/**
	 * 监听函数，有更新位置的时候，格式化成字符串，输出到屏幕中
	 */
	public class MyLocationListenner implements BDLocationListener {

		// private String result;

		@Override
		// 接收位置信息
		public void onReceiveLocation(BDLocation location) {

			if (location == null)
				return;
			array[0] = (float) location.getLatitude();
			array[1] = (float) location.getLongitude();
			if (LoginActivity.UserOrNot()) {
				d.run();

			} else {
				return;
			}
		}

		// 接收POI信息函数，我不需要POI，所以我没有做处理
		public void onReceivePoi(BDLocation poiLocation) {
			if (poiLocation == null) {
				return;
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public class sListenner implements Runnable {
		@Override
		public void run() {
			// 发送广播
			Intent intent = new Intent();
			intent.putExtra("count", array);
			intent.setAction("android.intent.action.test");
			sendBroadcast(intent);
		}
	}

}
