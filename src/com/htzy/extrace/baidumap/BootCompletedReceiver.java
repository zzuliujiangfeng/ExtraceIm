package com.htzy.extrace.baidumap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
/**
 * 接收手机状态
 * @author liujiangfeng
 *
 */
public class BootCompletedReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		context.startService(new Intent(context, RealTimeLocationServer.class));
	}

}
