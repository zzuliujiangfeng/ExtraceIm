package com.htzy.extrace.ui.misc;

import java.util.List;

import com.htzy.extrace.misc.model.CustomerInfo;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.ui.main.*;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomerListAdapter extends ArrayAdapter<CustomerInfo> implements
		IDataAdapter<List<CustomerInfo>> {

	private List<CustomerInfo> itemList;
	private Context context;

	// private GenFilter filter;

	public CustomerListAdapter(List<CustomerInfo> itemList, Context ctx) {
		super(ctx, R.layout.customer_select_list, itemList);
		this.itemList = itemList;
		this.context = ctx;
	}

	public int getCount() {
		if (itemList != null)
			return itemList.size();
		return 0;
	}

	public CustomerInfo getItem(int position) {
		if (itemList != null)
			return itemList.get(position);
		return null;
	}

	public void setItem(CustomerInfo ci, int position) {
		if (itemList != null)
			itemList.set(position, ci);
	}

	public long getItemId(int position) {
		if (itemList != null)
			return itemList.get(position).hashCode();
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.customer_select_list, null);
		}

		CustomerInfo c = itemList.get(position);
		TextView text = (TextView) v.findViewById(android.R.id.text1);
		text.setText(c.getName());
		text.setTag(position);
		return v;
	}

	@Override
	public List<CustomerInfo> getData() {
		return itemList;
	}

	@Override
	public void setData(List<CustomerInfo> data) {
		this.itemList = data;
		for (CustomerInfo c : data) {
			System.out.println(c);
		}
	}
}