package com.htzy.extrace.ui.domain;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.htzy.extrace.loader.ExpressListLoader;
import com.htzy.extrace.misc.model.ExpressSheet;
import com.htzy.extrace.ui.main.ExTraceApplication;
import com.htzy.extrace.ui.main.MainActivity;
import com.htzy.extrace.ui.main.R;
import com.htzy.extrace.util.Global;

public class InformActivity extends ActionBarActivity {

	PlaceholderFragment list_fg;
	public static List<ExpressSheet> ess;
	public static ArrayList<String> tels;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		FragmentManager fm = getSupportFragmentManager();
		if (fm.findFragmentById(android.R.id.content) == null) {
			list_fg = new PlaceholderFragment();
			fm.beginTransaction().add(android.R.id.content, list_fg).commit();
		}
		tels = new ArrayList<String>();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		list_fg.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * 返回或保存时调用的方法,返回到主activity
	 */
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		}
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// 左上角返回按钮监听
		if (item.getItemId() == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends ListFragment {
		private InformAdapter mAdapter;
		private ExpressListLoader mLoader;

		private ExpressSheet selectItem;
		private int selectPosition;
		Intent mIntent;
		ListView v;

		public PlaceholderFragment() {
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			setHasOptionsMenu(true);

			mAdapter = new InformAdapter(new ArrayList<ExpressSheet>(),
					this.getActivity());
			setListAdapter(mAdapter);
			getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			v = getListView();
			registerForContextMenu(getListView());
			mLoader = new ExpressListLoader(mAdapter, this.getActivity());
			mLoader.LoadExpressListInPackage(ExTraceApplication.getUserInfo()
					.getDelivePackageID());
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			mIntent = activity.getIntent();
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.inform, menu);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			switch (id)
				{
				case R.id.action_select_all: {
					tels = new ArrayList<String>();
					int len = mAdapter.getCount();
					String tel = null;
					for(int i = 0; i < len; i ++){
						tel = ess.get(i).getRecever().getTelCode();
						tels.add(tel);
						v.setItemChecked(i, true);
						
					}
					break;
				}
				case R.id.action_select_none: {
					tels = new ArrayList<String>();
					int len = mAdapter.getCount();
					for(int i = 0; i < len; i ++){
						v.setItemChecked(i, false);
					}
					break;
				}
				case R.id.action_select_others: {
					//tels = new ArrayList<String>();
					int len = mAdapter.getCount();
					String tel = null;
					for(int i = 0; i < len; i ++){
						tel = ess.get(i).getRecever().getTelCode();
						if(tels.contains(tel)){
							tels.remove(tel);
							v.setItemChecked(i, false);
						}else{
							tels.add(tel);
							v.setItemChecked(i, true);
						}
					}
					break;
				}
				case R.id.info: {
					String sms_content = ExTraceApplication.getMsg();
					for (String s : tels) {
						String phone_number = s;
						if (ExTraceApplication.isISDEUBG()) {
							phone_number = "10086";
						}
						if (phone_number.equals("")) {
							Toast.makeText(getActivity(), "null number is err",
									Toast.LENGTH_LONG).show();
						} else {
							SmsManager smsManager = SmsManager.getDefault();
							if (sms_content.length() > 70) {
								List<String> contents = smsManager
										.divideMessage(sms_content);
								for (String sms : contents) {
									smsManager.sendTextMessage(phone_number,
											null, sms, null, null);
								}
							} else {
								smsManager.sendTextMessage(phone_number, null,
										sms_content, null, null);
							}
							Toast.makeText(getActivity(), "success",
									Toast.LENGTH_SHORT).show();
						}

						Toast.makeText(getActivity(), "发送成功:)",
								Toast.LENGTH_SHORT).show();
					}
				}
				}
			System.out.println(tels);
			return super.onOptionsItemSelected(item);
		}

		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			String tel = ess.get(position).getRecever().getTelCode();
			if (tels.contains(tel)) {
				tels.remove(tel);
			} else {
				tels.add(tel);
			}
			System.out.println(tels);
		}
	}
	
}
