package com.htzy.extrace.ui.domain;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.htzy.extrace.loader.TransPackageLoader;
import com.htzy.extrace.misc.model.TransPackage;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.ui.main.R;
import com.zxing.activity.CaptureActivity;

public class TransPackageEditActivity extends ActionBarActivity implements
		IDataAdapter<TransPackage> {

	/**
	 * 拆包
	 */
	public static final int TRANSPACKAGE_UNPACK = 100;
	/**
	 * 打包
	 */
	public static final int TRANSPACKAGE_PACK = 101;
	/**
	 * 派送
	 */
	public static final int TRANSPACKAGE_SEND = 102;

	/**
	 * 签收
	 */
	public static final int TRANSPACKAGE_SIGNED = 103;

	private Intent mIntent;
	private TransPackageLoader mLoader;
	private ArrayList<String> sheetIds = new ArrayList<String>();
	ListView list;
	ArrayAdapter<String> adapter;
	// 转运包裹
	private String transpackage;
	private TransPackage transPackage;

	public TransPackage getTransPackage() {
		return transPackage;
	}

	public void setTransPackage(TransPackage transPackage) {
		this.transPackage = transPackage;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transpackage_edit);

		list = (ListView) findViewById(R.id.transpackage);

		mIntent = getIntent();
		if (mIntent.hasExtra("Action")) {
			if (mIntent.getStringExtra("Action").equals("Unpack")) {
				Toast.makeText(this, "请扫描包裹号:", Toast.LENGTH_SHORT).show();
				StartCapture(TRANSPACKAGE_UNPACK);
			} else if (mIntent.getStringExtra("Action").equals("Pack")) {
				StartCapture(TRANSPACKAGE_PACK);
			} else if (mIntent.getStringExtra("Action").equals("Send")) {
				StartCapture(TRANSPACKAGE_SEND);
			} else if (mIntent.getStringExtra("Action").equals("Signed")) {
				StartCapture(TRANSPACKAGE_SIGNED);
			} else {
				this.setResult(RESULT_CANCELED, mIntent);
				this.finish();
			}
		} else {
			this.setResult(RESULT_CANCELED, mIntent);
			this.finish();
		}

	}

	@Override
	public TransPackage getData() {
		return null;
	}

	@Override
	public void setData(TransPackage transPackage) {
		if (transPackage == null) {
			finish();
			return;
		}
		this.transPackage = transPackage;
	}

	@Override
	public void notifyDataSetChanged() {
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, sheetIds);
		list.setAdapter(adapter);
	}

	/**
	 * 开启相机
	 */
	private void StartCapture(int state) {
		Intent intent = new Intent();
		intent.putExtra("Action", "Captrue");
		intent.setClass(this, CaptureActivity.class);
		startActivityForResult(intent, state);
	}

	/**
	 * 得到扫描后的查询结果
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data == null) {
			if (sheetIds.size() == 0) {
				finish();
			}
			return;
		}
		switch (requestCode)
			{
			case TRANSPACKAGE_UNPACK: {
				if (data.hasExtra("result")) {
					if (transpackage == null) {
						// 得到要拆包的包裹号
						transpackage = data.getExtras().getString("result");
						StartCapture(TRANSPACKAGE_UNPACK);
						Toast.makeText(this, "请扫描快递单号:)", Toast.LENGTH_SHORT);
					} else {
						// 得到扫描后的快件序列号
						String sheet = null;
						sheet = data.getExtras().getString("result");
						mLoader = new TransPackageLoader(this, this);
						mLoader.unPack(transpackage, sheet);
						sheetIds.add(sheet);
						Toast.makeText(this, "包裹" + sheet + "拆包",
								Toast.LENGTH_SHORT).show();
						StartCapture(TRANSPACKAGE_UNPACK);
					}
				}
				break;
			}
			case TRANSPACKAGE_PACK: {
				if (data.hasExtra("result")) {
					String expressheetID = null;
					expressheetID = data.getExtras().getString("result");
					mLoader = new TransPackageLoader(this, this);
					mLoader.pack(expressheetID);
					sheetIds.add(expressheetID);
					StartCapture(TRANSPACKAGE_PACK);
				}
				break;
			}
			case TRANSPACKAGE_SEND: {
				if (data.hasExtra("result")) {
					String expressheetID = null;
					expressheetID = data.getExtras().getString("result");
					mLoader = new TransPackageLoader(this, this);
					mLoader.send(expressheetID);
					sheetIds.add(expressheetID);
					StartCapture(TRANSPACKAGE_SEND);
				}
				break;
			}
			case TRANSPACKAGE_SIGNED: {
				if (data.hasExtra("result")) {
					String expressheetID = null;
					expressheetID = data.getExtras().getString("result");
					mLoader = new TransPackageLoader(this, this);
					mLoader.signed(expressheetID);
					sheetIds.add(expressheetID);
					StartCapture(TRANSPACKAGE_SIGNED);
				}
				break;
			}
			default: {
				break;
			}
			}
	}

}
