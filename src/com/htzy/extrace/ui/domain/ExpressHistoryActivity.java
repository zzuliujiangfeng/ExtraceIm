package com.htzy.extrace.ui.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.htzy.extrace.loader.ExpressHistoryLoader;
import com.htzy.extrace.misc.model.HistoryGist;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.ui.main.R;

public class ExpressHistoryActivity extends ActionBarActivity implements
		IDataAdapter<List<HistoryGist>> {

	ArrayAdapter<String> adapter;
	private HistoryGist historyGist;
	private Intent mIntent;
	private ExpressHistoryLoader mLoader;
	private List<HistoryGist> historyGists;
	private ArrayList<String> hStrings;
	ListView list;

	public HistoryGist getHistoryGist() {
		return historyGist;
	}

	public void setHistoryGist(HistoryGist historyGist) {
		this.historyGist = historyGist;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_express_history);
		list = (ListView) findViewById(R.id.express_history);
		hStrings = new ArrayList<String>();
		//
		mIntent = getIntent();
		if (mIntent.hasExtra("ExpressId")) {
			String expressId = mIntent.getStringExtra("ExpressId");
			mLoader = new ExpressHistoryLoader(this, this);
			mLoader.getHistory(expressId);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.express_history, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void notifyDataSetChanged() {
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, hStrings);
		if (list != null) {
			list.setAdapter(adapter);
		}
	}

	@Override
	public List<HistoryGist> getData() {
		return null;
	}

	@Override
	public void setData(List<HistoryGist> data) {
		historyGists = data;
		for (HistoryGist h : historyGists) {
			if (h != null) {
				String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(h.getTime());
				String nodename = h.getNodeName();
				String stat = null;
				switch (h.getState())
					{
					case HistoryGist.STATE.HISTORYGIST_RECEIVE: {
						stat = "揽收";
						break;
					}
					case HistoryGist.STATE.HISTORYGIST_TRANS: {
						stat = "中转";
						break;
					}
					case HistoryGist.STATE.HISTORYGIST_TARGET: {
						stat = "终点";
						break;
					}
					//case HistoryGist.STATE.HISTORYGIST_NOW: {
						//stat = "实时位置";
						//break;
					//}
					}
				if(h.getState() != HistoryGist.STATE.HISTORYGIST_NOW){
					hStrings.add(time + ":" + nodename + ":" + stat);
				}
			}
		}
	}
}
