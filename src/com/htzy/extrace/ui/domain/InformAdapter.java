package com.htzy.extrace.ui.domain;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.htzy.extrace.misc.model.ExpressSheet;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.ui.main.R;

public class InformAdapter extends ArrayAdapter<ExpressSheet> implements
		IDataAdapter<List<ExpressSheet>> {

	private List<ExpressSheet> itemList;
	private Context context;

	public InformAdapter(List<ExpressSheet> itemList, Context ctx) {
		super(ctx, R.layout.activity_inform, itemList);
		this.itemList = itemList;
		this.context = ctx;
	}

	public int getCount() {
		if (itemList != null)
			return itemList.size();
		return 0;
	}

	public ExpressSheet getItem(int position) {
		if (itemList != null)
			return itemList.get(position);
		return null;
	}

	public void setItem(ExpressSheet ci, int position) {
		if (itemList != null)
			itemList.set(position, ci);
	}

	public long getItemId(int position) {
		if (itemList != null)
			return itemList.get(position).hashCode();
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.activity_inform, null);
		}

		ExpressSheet c = itemList.get(position);
		TextView text = (TextView) v.findViewById(android.R.id.text1);
		text.setText(c.getRecever().getName() + "  " + c.getRecever().getAddress());
		text.setTag(position);
		return v;
	}

	@Override
	public List<ExpressSheet> getData() {
		return itemList;
	}

	@Override
	public void setData(List<ExpressSheet> data) {
		this.itemList = data;
		InformActivity.ess=data;
	}
}