package com.htzy.extrace.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * 欢迎页面
 * 
 * @author liujiangfeng
 *
 */
public class WelcomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);

		new Handler().postDelayed(new Runnable() {

			public void run() {
				Intent intent = new Intent();
				if (!LoginActivity.UserOrNot()) {
					intent.setClass(WelcomeActivity.this,
							TouristMainActivity.class);
					WelcomeActivity.this.startActivity(intent);
					WelcomeActivity.this.finish();
				} else {
					intent.setClass(WelcomeActivity.this, MainActivity.class);
					WelcomeActivity.this.startActivity(intent);
					WelcomeActivity.this.finish();
				}
			}
		}, 1000);

	}

}