package com.htzy.extrace.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.htzy.extrace.util.Global;

/**
 * 手机在安装好APP后，可以对服务器的信息进行设置 可以设置发送短信的短信内容
 * 
 * @author mouse
 */
public class Setting extends ActionBarActivity {

	// 服务器路径
	String mServerUrl;
	// 短信内容
	String msgContent;
	EditText servertext;
	EditText senmsg;
	Button button_save_server_url;
	// 快捷设置按钮
	Button button_local_server;
	Button button_coding_server;
	Button button_oschina_server;
	// 短信内容保存按钮
	Button button_save_msg;
	ExTraceApplication eta;

	// debug模式
	TextView is_debug;
	Button is_debug_true;
	Button is_debug_false;

	// 实时位置上传时间
	EditText time;
	Button save_time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_setting);

		eta = new ExTraceApplication();

		// 获取配置信息显示在设置界面
		mServerUrl = eta.getServerUrl();
		// 获取界面输入框
		servertext = (EditText) findViewById(R.id.hosturl);
		senmsg = (EditText) findViewById(R.id.sendmsg);
		// 显示默认配置
		servertext.setText(mServerUrl);
		senmsg.setText(ExTraceApplication.getMsg().toString());

		// 获取各按钮
		button_save_server_url = (Button) findViewById(R.id.submit_button);
		button_local_server = (Button) findViewById(R.id.submit_button_server_local);
		button_coding_server = (Button) findViewById(R.id.submit_button_server_coding);
		button_oschina_server = (Button) findViewById(R.id.submit_button_server_oschina);
		button_save_msg = (Button) findViewById(R.id.save_msg);

		// 服务器路径保存按钮
		button_save_server_url.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerUrl = servertext.getText().toString();
				eta.setServerUrl(mServerUrl);
				onBackPressed();
			}
		});

		// 本地服务器快捷设置按钮
		button_local_server.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerUrl = Global.HOSTURL;
				eta.setServerUrl(mServerUrl);
				onBackPressed();
			}
		});

		// coding服务器快捷设置访问按钮
		button_coding_server.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerUrl = Global.CODING_URL;
				eta.setServerUrl(mServerUrl);
				onBackPressed();
			}
		});

		// 开源中国服务器快捷设置访问按钮
		button_oschina_server.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerUrl = Global.OSCHINA_URL;
				eta.setServerUrl(mServerUrl);
				onBackPressed();
			}
		});

		// 短信内容保存按钮
		button_save_msg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				msgContent = senmsg.getText().toString();
				eta.setSendMsg(msgContent);
				onBackPressed();
			}
		});

		is_debug = (TextView) findViewById(R.id.is_debug);
		is_debug.setText(ExTraceApplication.isISDEUBG() ? "是" : "否");
		is_debug_true = (Button) findViewById(R.id.is_debug_true);
		is_debug_true.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ExTraceApplication.setISDEUBG(true);
			}
		});
		is_debug_false = (Button) findViewById(R.id.is_debug_false);
		is_debug_false.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ExTraceApplication.setISDEUBG(false);
			}
		});

		time = (EditText) findViewById(R.id.timee);
		int t = ExTraceApplication.getTIME();
		time.setText(t+"");
		save_time = (Button) findViewById(R.id.save_time);
		save_time.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					int t = Integer.valueOf(time.getText().toString());
					ExTraceApplication.setTIME2(t);
				} catch (Exception e) {
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	/**
	 * 返回或保存时调用的方法,返回到主activity
	 */
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		}
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// 左上角返回按钮监听
		if (item.getItemId() == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
