package com.htzy.extrace.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.htzy.extrace.misc.model.UserInfo;
import com.htzy.extrace.ui.domain.ExpressEditActivity;
import com.htzy.extrace.ui.domain.TransPackageEditActivity;
import com.htzy.extrace.ui.misc.CustomerListActivity;

public class MainFragment extends Fragment {

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static MainFragment newInstance() {
		MainFragment fragment = new MainFragment();
		Bundle args = new Bundle();
		// args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// 功能选择主列表
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		/**
		 * 快件揽收按钮监听
		 */
		rootView.findViewById(R.id.action_ex_receive_layout)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						StartReceiveExpress();
					}
				});
		/**
		 * 快件派送按钮监听
		 */
		rootView.findViewById(R.id.action_ex_transfer_layout)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						// 为什么调用快件查询的方法，是一个方法吗？
						StartDlvExpress();
					}
				});
		/**
		 * 快件签收按钮监听
		 */
		rootView.findViewById(R.id.action_ex_signed_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						startSignedExpress();
					}
				});
		/**
		 * 包裹拆包按钮监听
		 */
		rootView.findViewById(R.id.action_pk_exp_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						StartEditPackageUnpack();
					}
				});
		/**
		 * 包裹打包按钮监听
		 */
		rootView.findViewById(R.id.action_pk_pkg_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						StartEditPackagePack();
					}
				});
		/**
		 * 客户管理按钮监听
		 */
		rootView.findViewById(R.id.action_cu_mng_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						StartCustomerList();
					}
				});
		/**
		 * 快件查询按钮监听
		 */
		rootView.findViewById(R.id.action_ex_qur_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						StartQueryExpress();
					}
				});

		return rootView;
	}

	/**
	 * 快件揽收方法执行
	 */
	void StartReceiveExpress() {
		if (!validate()) {
			return;
		}
		String packageID = ExTraceApplication.getUserInfo().getReceivePackageID();
		if (packageID != null && packageID.length() > 0) {
			Intent intent = new Intent();
			intent.putExtra("Action", "New");
			intent.setClass(this.getActivity(), ExpressEditActivity.class);
			startActivityForResult(intent, 0);
		}else{
			Toast.makeText(getActivity(), "您没有揽收包裹:(", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 解释startActivityForResult()：
	 * startActivityForResult()可以一次性完成这项任务，当程序执行到这段代码的时候，
	 * 假若从T1Activity跳转到下一个Text2Activity，而当这个Text2Activity调用了
	 * finish()方法以后，程序会自动跳转回T1Activity，并调用前一个T1Activity中的 onActivityResult( )方法。
	 */

	/**
	 * 快件派送方法执行
	 */
	void StartDlvExpress() {
		if (!validate()) {
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("Action", "Send");
		intent.setClass(this.getActivity(), TransPackageEditActivity.class);
		startActivityForResult(intent, 0);
	}

	/**
	 * 快件签收方法执行
	 */
	void startSignedExpress() {
		if (!validate()) {
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("Action", "Signed");
		intent.setClass(this.getActivity(), TransPackageEditActivity.class);
		startActivityForResult(intent, 0);
	}

	/**
	 * 包裹拆包方法执行
	 */
	void StartEditPackageUnpack() {
		if (!validate()) {
			return;
		}
		if (!checkURull()) {
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("Action", "Unpack");
		intent.setClass(this.getActivity(), TransPackageEditActivity.class);
		startActivityForResult(intent, 0);

	}

	/**
	 * 包裹打包方法执行
	 */
	void StartEditPackagePack() {
		if (!validate()) {
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("Action", "Pack");
		intent.setClass(this.getActivity(), TransPackageEditActivity.class);
		startActivityForResult(intent, 0);
		return;
	}

	/**
	 * 客户管理方法执行
	 */
	void StartCustomerList() {
		if (!validate()) {
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("Action", "None");
		intent.setClass(this.getActivity(), CustomerListActivity.class);
		startActivityForResult(intent, 0);

	}

	/**
	 * 快件查询方法执行
	 */
	void StartQueryExpress() {
		Intent intent = new Intent();
		intent.putExtra("Action", "Query");
		intent.setClass(this.getActivity(), ExpressEditActivity.class);
		startActivityForResult(intent, 0);

	}

	/**
	 * 验证是否是已经登陆的用户，除查询快件外，非登陆的用户都不可以操作
	 * 
	 * @return
	 */
	boolean validate() {
		if (LoginActivity.UserOrNot()) {
			// 已经登陆用户
			return true;
		} else {
			// 未登陆用户
			Toast.makeText(this.getActivity(), "登录才可以操作呦！:)",
					Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	/**
	 * 检验登陆的用户是否是管理员,如果不是管理员提示信息
	 * 
	 * @return 管理员true，非管理员false
	 */
	boolean checkURull() {
		if (ExTraceApplication.getUserInfo().getURull() != UserInfo.URULL.URULL_MANAGER) {
			Toast.makeText(getActivity(), "您不是管理员,没有权限拆包:(", Toast.LENGTH_SHORT)
					.show();
			return false;
		} else {
			return true;
		}
	}
}
