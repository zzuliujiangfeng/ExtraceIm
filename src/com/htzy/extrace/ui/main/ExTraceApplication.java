package com.htzy.extrace.ui.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.htzy.extrace.misc.model.UserInfo;
import com.htzy.extrace.util.Global;

/**
 * 程序启动，初始化配置，读入服务器访问路径，Misc和Domain访问路径，获取已经登陆的用户信息 获取设置的默认发送短信的内容
 */
public class ExTraceApplication extends Application {
	private static final String PREFS_NAME = Global.PREFS_NAME;
	private static final String CONFIG_USER = Global.CONFIG_USER;
	public static String mServerUrl;
	public static String mMiscService, mDomainService, mUserInfoService,
			mLoginService;
	public static String sendMsg;
	private static UserInfo userInfo;
	private static SharedPreferences settings;
	private static SharedPreferences usermsg;
	private static boolean ISDEUBG;
	/**
	 * 实时位置上传时间间隔
	 */
	private static int TIME = Global.TIME;

	/**
	 * @return 如果为debug模式返回true,否则返回false
	 */
	public static boolean isISDEUBG() {
		return ISDEUBG;
	}

	public static void setISDEUBG(boolean iSDEUBG) {
		ISDEUBG = iSDEUBG;
	}

	public static int getTIME() {
		return TIME;
	}

	public static UserInfo getUserInfo() {
		return userInfo;
	}

	public static void setUserInfo(UserInfo userInfo) {
		ExTraceApplication.userInfo = userInfo;
	}

	public String getServerUrl() {
		return mServerUrl;
	}

	public String getMiscServiceUrl() {
		return mServerUrl + mMiscService;
	}

	public String getDomainServiceUrl() {
		return mServerUrl + mDomainService;
	}

	public String getLoginServiceUrl() {
		return mServerUrl + mLoginService;
	}

	public static String getMsg() {
		return sendMsg;
	}

	/**
	 * 在程序其他地方要调用这个方法得到已经登陆的userInfo信息，以便更多的操作，其中不包括密码
	 * 
	 * @return 已经登陆的用户信息userInfo
	 */
	public UserInfo getLoginUser() {
		return userInfo;
	}

	/**
	 * 设置服务器路径，在设置中进行设置后保存
	 * 
	 * @param url
	 */
	public void setServerUrl(String url) {
		mServerUrl = url;
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("ServerUrl", mServerUrl);
		editor.commit();
	}

	/**
	 * 设置发送短信的短信内容，在设置中进行设置后保存
	 * 
	 * @param msg
	 */
	public void setSendMsg(String msg) {
		sendMsg = msg;
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("SendMsg", sendMsg);
		editor.commit();
	}
	
	public void setISISDEBUG(boolean debug) {
		ISDEUBG = debug;
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("ISDEBUG", ISDEUBG);
		editor.commit();
	}
	
	public static void setTIME2(int time) {
		TIME = time;
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("TIME", TIME);
		editor.commit();
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// 从配置文件中读配置数据
		settings = getSharedPreferences(PREFS_NAME, 0);
		mServerUrl = settings.getString("ServerUrl", "");
		//mServerUrl = settings.getString("ServerUrl",Global.CODING_URL);
		mServerUrl = settings.getString("ServerUrl", Global.CODING_URL);
		mMiscService = settings.getString("MiscService", "/cxf/rest/Misc/");
		mDomainService = settings.getString("DomainService",
				"/cxf/rest/Domain/");
		mLoginService = settings.getString("LoginService", "/cxf/user/");
		sendMsg = settings.getString("SendMsg", "你好,海豚之翼卓越快递,郑大南门在等你,来吧！");
		ISDEUBG = settings.getBoolean("ISDEBUG", Global.ISDEUBG);
		TIME = settings.getInt("TIME", Global.TIME);
		
		// 如果是第一次运行该程序，则进行默认配置
		if (mServerUrl == null || mServerUrl.length() == 0) {
			mServerUrl = Global.HOSTURL;
			//mServerUrl = Global.CODING_URL;
			// mServerUrl = Global.HOSTURL;
			mServerUrl = Global.CODING_URL;
			mMiscService = "/cxf/rest/Misc/";
			mDomainService = "/cxf/rest/Domain/";  
			mLoginService = "/cxf/user/";

			SharedPreferences.Editor editor = settings.edit();
			editor.putString("ServerUrl", mServerUrl);
			editor.putString("MiscService", mMiscService);
			editor.putString("DomainService", mDomainService);
			editor.putString("LoginService", mLoginService);
			editor.putString("SendMsg", "你好,海豚之翼卓越快递,郑大南门在等你,来吧！");
			editor.putBoolean("ISDEBUG", Global.ISDEUBG);
			editor.putInt("TIME", Global.TIME);
			editor.commit();
		}

		// 读入登陆用户配置文件中的信息
		usermsg = getSharedPreferences(CONFIG_USER, 0);
		userInfo = new UserInfo();
		InitUserMsg();
		if (userInfo.getName() == null || userInfo.getName().length() == 0) {
			Toast.makeText(this, "查询信息等需要登录！", Toast.LENGTH_LONG).show();
		} else {
			LoginActivity.setUserInfo(userInfo);
		}
	}

	public void onTerminate() {
		super.onTerminate();
	}

	/**
	 * 每次运行，读入用户信息以备用
	 */
	private void InitUserMsg() {
		userInfo.setID(usermsg.getInt("UID", 0));
		userInfo.setName(usermsg.getString("name", null));
		userInfo.setURull(usermsg.getInt("URull", 0));
		userInfo.setTelCode(usermsg.getString("telCode", null));
		userInfo.setStatus(usermsg.getInt("status", 0));
		userInfo.setDptID(usermsg.getString("dptID", null));
		userInfo.setReceivePackageID(usermsg
				.getString("receivePackageID", null));
		userInfo.setDelivePackageID(usermsg.getString("delivePackageID", null));
		userInfo.setTransPackageID(usermsg.getString("transPackageID", null));
	}
}
