package com.htzy.extrace.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.htzy.extrace.util.Global;

public class LogoutActivity extends ActionBarActivity {

	private static final String PREFS_NAME = Global.CONFIG_USER;
	private SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		
		// 清空登陆用户的信息
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt("UID", 0);
		editor.putString("name", "");
		editor.putInt("URull", 0);
		editor.putString("telCode", "");
		editor.putInt("status", 0);
		editor.putString("dptID", "");
		editor.putString("receivePackageID", "");
		editor.putString("delivePackageID", "");
		editor.putString("transPackageID", "");
		editor.commit();
		// 跳转到登陆界面
		loginIntent();
	}

	private void loginIntent() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		finish();
	}
}
