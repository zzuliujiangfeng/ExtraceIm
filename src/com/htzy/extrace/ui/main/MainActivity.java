package com.htzy.extrace.ui.main;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.htzy.extrace.baidumap.BaiduMapActivity;
import com.htzy.extrace.baidumap.RealTimeLocationServer;
import com.htzy.extrace.loader.PackageRouteLoader;
import com.htzy.extrace.misc.model.PackageRoute;
import com.htzy.extrace.net.IDataAdapter;
import com.htzy.extrace.ui.domain.ExpressEditActivity;
import com.htzy.extrace.ui.domain.ExpressListFragment;
import com.htzy.extrace.ui.domain.ExpressListFragment.OnFragmentInteractionListener;
import com.htzy.extrace.ui.domain.InformActivity;

public class MainActivity extends ActionBarActivity implements
		ActionBar.TabListener, OnFragmentInteractionListener,
		IDataAdapter<List<PackageRoute>> {

	SectionsPagerAdapter mSectionsPagerAdapter;
	ViewPager mViewPager;
	public float latitude;
	public float longitude;
	private MyReceiver receiver = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if(!LoginActivity.UserOrNot()){
			Intent intent = new Intent();
			intent.setClass(this, TouristMainActivity.class);
			startActivity(intent);
			finish();
			return;
		}
		
		if (!ExTraceApplication.isISDEUBG()) {
			// 启动服务
			startService(new Intent(this, RealTimeLocationServer.class));
			// 注册广播接收器
			receiver = new MyReceiver();
			IntentFilter filter = new IntentFilter();
			filter.addAction("android.intent.action.test");
			MainActivity.this.registerReceiver(receiver, filter);
		}

		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// 设置ViewPager切换时候的监听事件
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						// 页面滑动，顶部标签跟着改变
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// 在actionBar上添加tab，并为每一个tab添加被选择后的回调事件
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

		// 总是显示overflow按钮，消除部分手机不显示overflow按钮的情况
		setOverflowShowingAlways();
	}

	/**
	 * 在ViewConfiguration这个类中有一个叫做sHasPermanentMenuKey的静态变量，
	 * 系统就是根据这个变量的值来判断手机有没有物理Menu键的。 当然这是一个内部变量，我们无法直接访问它，
	 * 但是setOverflowShowingAlways
	 * ()内部就是通过反射的方式修改sHasPermanentMenuKey的值，让它永远为false就可以了
	 */
	private void setOverflowShowingAlways() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 方法说明： Activity启动的时候，系统会调用Activity的onCreateOptionsMenu()方法来取出所有的Action按钮
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// 调用了MenuInflater的inflate()方法来加载menu资源就
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * 方法说明： 当用户点击Action按钮的时候，系统会调用Activity的onOptionsItemSelected()方法，
	 * 通过方法传入的MenuItem参数，
	 * 我们可以调用它的getItemId()方法和menu资源中的id进行比较，从而辨别出用户点击的是哪一个Action按钮
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		switch (id)
			{
			case R.id.action_search: {
				return true;
			}
			case R.id.action_new: {
				if (!validate()) {
					return false;
				}
				String packageID = ExTraceApplication.getUserInfo()
						.getReceivePackageID();
				if (packageID != null && packageID.length() > 0) {
					Intent intent = new Intent();
					intent.putExtra("Action", "New");
					intent.setClass(this, ExpressEditActivity.class);
					startActivityForResult(intent, 0);
				} else {
					Toast.makeText(this, "您没有揽收包裹:(", Toast.LENGTH_SHORT)
							.show();
				}
				return true;
			}
			case R.id.action_my_package: {
				// 先判断是否登录，若登录，则显示包裹列表
				if (LoginActivity.UserOrNot()) {
					// 已经登陆
					Intent intent_my_package = new Intent(this,
							MyPackageActivity.class);
					startActivity(intent_my_package);
					finish();
				} else {
					// 未登录，提示用户先登录，并返回登录界面
					Toast.makeText(this, "请您先登录", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(this, LoginActivity.class);
					startActivity(intent);
					finish();
				}
				return true;
			}
			case R.id.action_my_message: {
				return true;
			}
			case R.id.action_my_location: {
				// 点击我的位置，显示我的当前位置
				Intent intent_baidu = new Intent(this, BaiduMapActivity.class);
				startActivity(intent_baidu);
				finish();
				return true;
			}
			case R.id.action_logout: {
				if (LoginActivity.UserOrNot()) {
					// 点击退出按钮时，跳转到登录界面
					AlertDialog.Builder ad = new AlertDialog.Builder(this);
					/*
					 * setTitle()：给对话框设置title. setIcon():给对话框设置图标。
					 * setMessage():设置对话框的提示信息
					 * setItems()：设置对话框要显示的一个list,一般用于要显示几个命令时
					 * setSingleChoiceItems():设置对话框显示一个单选的List
					 * setMultiChoiceItems():用来设置对话框显示一系列的复选框。
					 * setPositiveButton():给对话框添加”Yes”按钮。
					 * setNegativeButton():给对话框添加”No”按钮。 show():显示对话框，一般放最后
					 */
					ad.setTitle("");
					ad.setMessage("确定退出？");
					ad.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int i) {
									logoutIntent();
								}
							});
					ad.setNegativeButton("取消", null);
					ad.show();
					return true;
				} else {
					logoutIntent();
				}
				return true;
			}
			case R.id.action_inform: {
				// 点击设置按钮时，跳转到设置界面
				Intent intent = new Intent(this, InformActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				return true;
			}
			case R.id.action_settings:
				// 点击设置按钮时，跳转到设置界面
				Intent intent = new Intent(this, Setting.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				return true;
			}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * tab被选中
	 */
	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// 但选择了一个tab，则激发相匹配的动作
		mViewPager.setCurrentItem(tab.getPosition());
	}

	/**
	 * tab没有被选中
	 */
	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * tab再次被选中
	 */
	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		/**
		 * 生成新的Fragment对象
		 */
		@Override
		public Fragment getItem(int position) {
			switch (position)
				{
				case 0:
					return MainFragment.newInstance();
				case 1:
					return ExpressListFragment.newInstance("ExDLV"); // 派送快件
				case 2:
					return ExpressListFragment.newInstance("ExRCV"); // 揽收快件
				case 3:
					return ExpressListFragment.newInstance("ExTAN"); // 转运快件
				}
			return null;
		}

		/**
		 * 返回导航的的fragment数量 总共4页.
		 */
		@Override
		public int getCount() {
			return 4;
		}

		/**
		 * 得到四个功能选择的标题名称 功能选择，派送任务，揽收任务，转运任务
		 */
		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position)
				{
				case 0:
					return getString(R.string.title_section1).toUpperCase(l);
				case 1:
					return getString(R.string.title_section2).toUpperCase(l);
				case 2:
					return getString(R.string.title_section3).toUpperCase(l);
				case 3:
					return getString(R.string.title_section4).toUpperCase(l);
				}
			return null;
		}
	}

	@Override
	public void onFragmentInteraction(String id) {
	}

	/**
	 * 退出系统
	 */
	private void logoutIntent() {
		Intent intent = new Intent(this, LogoutActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	protected void onDestroy() {
		// 结束服务
		stopService(new Intent(MainActivity.this, RealTimeLocationServer.class));
		super.onDestroy();
	}

	/**
	 * 获取广播数据
	 * 
	 * @author liujiangfeng
	 */
	public class MyReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			float count[] = bundle.getFloatArray("count");
			latitude = count[0];
			longitude = count[1];
			savePosion(latitude, longitude);
		}
	}

	/**
	 * 将实时位置的经纬度传输到服务器
	 * 
	 * @param mLatitude
	 * @param mLongtitude
	 * @return
	 */
	private boolean savePosion(float latitude, float longtitude) {

		PackageRouteLoader position = new PackageRouteLoader(this, this);
		position.saveCurrentPosition(latitude, longtitude);
		return true;
	}

	@Override
	public List<PackageRoute> getData() {
		return null;
	}

	@Override
	public void setData(List<PackageRoute> data) {

	}

	@Override
	public void notifyDataSetChanged() {

	}

	/**
	 * 验证是否是已经登陆的用户，除查询快件外，非登陆的用户都不可以操作
	 * 
	 * @return
	 */
	boolean validate() {
		if (LoginActivity.UserOrNot()) {
			// 已经登陆用户
			return true;
		} else {
			// 未登陆用户
			Toast.makeText(this, "登录才可以操作呦！:)", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

}
