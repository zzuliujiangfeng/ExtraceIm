package com.htzy.extrace.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.htzy.extrace.ui.domain.ExpressEditActivity;

/**
 * 游客(未登录的用户)查询主界面
 * 
 * @author liujiangfeng
 *
 */
public class TouristMainActivity extends Activity {

	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题
		setContentView(R.layout.tourist_activity_main);

		if(LoginActivity.UserOrNot()){
			Intent intent = new Intent();
			intent.setClass(this, MainActivity.class);
			startActivity(intent);
			finish();
			return;
		}
		
		/*
		 * 游客查询主页的登录按钮或登录图标事件，
		 */
		findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(TouristMainActivity.this,
						LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});

		findViewById(R.id.loginpicture).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						Intent intent = new Intent(TouristMainActivity.this,
								LoginActivity.class);
						startActivity(intent);
						finish();
					}
				});

		/**
		 * 主页中输入快件的序列号后，点击查询按钮触发的事件
		 */
		findViewById(R.id.search).setOnClickListener(
				new View.OnClickListener() {
					private Context context;

					@Override
					public void onClick(View v) {

						Intent intent = new Intent();
						intent.setClass(TouristMainActivity.this,
								ExpressEditActivity.class);

						EditText sn = (EditText) findViewById(R.id.sn);
						String str = sn.getText().toString();
						if (str.equals("")) {
							Toast.makeText(TouristMainActivity.this,
									"快件序列号不能为空", Toast.LENGTH_SHORT).show();
							return;

						}
						Bundle bundle = new Bundle();
						intent.putExtra("Action", "touristResult");
						bundle.putString("touristResult", str);

						intent.putExtras(bundle);

						startActivityForResult(intent, 0);
						finish();
						return;
					}
				});

		/*
		 * 主页中查询栏中的扫描快件序列号按钮
		 */
		findViewById(R.id.camera).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						Intent intent = new Intent(TouristMainActivity.this,
								ExpressEditActivity.class);
						Bundle bundle = new Bundle();
						bundle.putString("Action", "Query");
						// 将Bundle对象传递给Intent
						intent.putExtras(bundle);
						// 启动另一个Activity页面
						startActivityForResult(intent, 0);
						finish();
						return;
					}
				});

		findViewById(R.id.tourist).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(TouristMainActivity.this,
								Setting.class);
						startActivity(intent);
						finish();
						return;
					}
				});
	}

}